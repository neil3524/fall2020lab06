package application;

import java.util.Random;

/**
 * @author Neil Fisher (1939668)
 */
public class RpsGame {
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;
	private Random rand = new Random();

	public int getWins() {
		return wins;
	}

	public int getTies() {
		return ties;
	}

	public int getLosses() {
		return losses;
	}

	public String playRound(String playerChoice) {
		String cpuChoice = "";
		switch (rand.nextInt(3)) {
		case 0:
			cpuChoice = "rock";
			break;
		case 1:
			cpuChoice = "paper";
			break;
		case 2:
			cpuChoice = "scissors";
			break;
		}

		if (playerChoice.equals("rock") && cpuChoice.equals("paper")) {
			losses++;
			return "Cpu plays paper and wins";
		} else if (playerChoice.equals("rock") && cpuChoice.equals("scissors")) {
			wins++;
			return "Cpu plays scissors and lost";
		} else if (playerChoice.equals("paper") && cpuChoice.equals("rock")) {
			wins++;
			return "Cpu plays rock and lost";
		} else if (playerChoice.equals("paper") && cpuChoice.equals("scissors")) {
			losses++;
			return "Cpu plays scissors and wins";
		} else if (playerChoice.equals("scissors") && cpuChoice.equals("rock")) {
			losses++;
			return "Cpu plays rock and wins";
		} else if (playerChoice.equals("scissors") && cpuChoice.equals("paper")) {
			wins++;
			return "Cpu plays paper and lost";
		}
		ties++;
		return "Tie with " + cpuChoice; // tie
	}
}
