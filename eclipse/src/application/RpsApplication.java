package application;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * @author Neil Fisher (1939668)
 */
public class RpsApplication extends Application {
	private RpsGame rps = new RpsGame();

	@Override
	public void start(Stage stage) {
		stage.setTitle("Rock Paper Scissors by Neil Fisher");

		Group root = new Group();

		Scene scene = new Scene(root, 800, 300);
		scene.setFill(Color.BLACK);
		// creating nodes
		VBox Vnodes = new VBox();
		// buttons
		HBox buttons = new HBox();
		Button rock = new Button("rock");
		Button paper = new Button("paper");
		Button scissors = new Button("scissors");
		buttons.getChildren().addAll(rock, paper, scissors);
		// textfields
		HBox textFields = new HBox();
		TextField message = new TextField("Welcome!");
		message.setPrefWidth(scene.getWidth() / 4);
		message.setEditable(false);
		TextField wins = new TextField("wins: 0");
		wins.setPrefWidth(scene.getWidth() / 4);
		wins.setEditable(false);
		TextField losses = new TextField("losses: 0");
		losses.setPrefWidth(scene.getWidth() / 4);
		losses.setEditable(false);
		TextField ties = new TextField("ties: 0");
		ties.setPrefWidth(scene.getWidth() / 4);
		ties.setEditable(false);
		textFields.getChildren().addAll(message, wins, losses, ties);
		textFields.setLayoutY(26);

		Vnodes.getChildren().addAll(buttons, textFields);

		// event listeners
		rock.setOnAction(new RpsChoice(message, wins, losses, ties, "rock", rps));
		paper.setOnAction(new RpsChoice(message, wins, losses, ties, "paper", rps));
		scissors.setOnAction(new RpsChoice(message, wins, losses, ties, "scissors", rps));

		root.getChildren().add(Vnodes);
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
