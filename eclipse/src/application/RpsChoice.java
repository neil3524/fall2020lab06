package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * @author Neil Fisher (1939668)
 */
public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame rps;

	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String playerChoice,
			RpsGame rps) {
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.playerChoice = playerChoice;
		this.rps = rps;
	}

	@Override
	public void handle(ActionEvent event) {
		message.setText(rps.playRound(playerChoice)); // plays a round then sets message's text
		wins.setText("wins: " + rps.getWins());
		losses.setText("losses: " + rps.getLosses());
		ties.setText("ties: " + rps.getTies());
	}
}
